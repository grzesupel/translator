# Quick translator

A simple tool for quickly adding language versions to your web page.

## Installation

There is no dependency in this project and you can use your favorite tool to compile js files. I am using a parceljs:


```bash
npm i
npm start
```
