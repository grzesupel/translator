import en from "./en";
import pl from "./pl";

const LOCAL_STORAGE_TRANSLATOR_NAME = "languageVersion";
const languageVersions = {
  en,
  pl
};

const translate = languageVersion => {
  Object.keys(languageVersion).forEach(v => {
    const element = document.querySelector(`[data-translator=${v}]`);
    if (element) {
      element.innerText = languageVersion[v];
    }
  });
};

const setLanguageVersionByLocalStorage = () => {
  const languageVersionFromLS = localStorage.getItem(
    LOCAL_STORAGE_TRANSLATOR_NAME
  );

  let isSet = false;
  Object.keys(languageVersions).forEach(v => {
    if (languageVersionFromLS === v) {
      translate(languageVersions[v]);
      isSet = true;
    }
    if (!isSet) {
      translate(languageVersions.en);
    }
  });
};

const boundTriggerPerVersion = className => {
  Object.keys(languageVersions).forEach(v => {
    const languageSwitcher = document.querySelector(`.${className}${v}`);
    if (!languageSwitcher) {
      return;
    }
    languageSwitcher.addEventListener("click", () => {
      translate(languageVersions[v]);
      localStorage.setItem(LOCAL_STORAGE_TRANSLATOR_NAME, `${v}`);
    });
  });
};

export default {
  boundTriggerPerVersion,
  setLanguageVersionByLocalStorage
};
